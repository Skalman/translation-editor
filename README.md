# Translation editor

The Translation editor is a cross-wiki gadget, for adding Wiktionary translations. For more information, see the [project wiki](https://gitlab.com/Skalman/translation-editor/wikis/home).

## Run locally

You can run this project locally, without editing a wiki.

1. Prerequisites:
   - Install [Git](https://git-scm.com/)
   - Install [Node.js](https://nodejs.org/)
2. Clone this project
   - Start a terminal with Git
   - Navigate to a folder you wish to use as a parent folder (e.g. `cd path/to/parent`)
   - Run `git clone https://gitlab.com/Skalman/translation-editor.git`
3. Install dependencies:
   - Start the terminal
   - Go to the main folder (e.g. `cd path/to/this/project`)
   - Run `npm install`
4. Run app:
   - Start the terminal
   - Go to the main folder
   - Run `npm start`

## Development

You should run Prettier and ESLint before committing your code. These are combined into the following command:

```sh
npm run precommit
```

There are several other Npm scripts useful during development: `start`, `prettier`, `lint`, `lint-fix`.

### Development environment

You can use any editor, but e.g. the following setup will ensure good productivity.

- [VS Code](https://code.visualstudio.com/)
  - Extension: [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
    - Shows lint errors directly in the editor.
  - Extension: [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
    - Allows you to reformat any JS, CSS, HTML, JSON and Markdown file.
  - Extension: [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)
    - Ensures that the editor will use correct indentation etc. without changing any settings.
