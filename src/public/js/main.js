/* global
	$,
	mw,
	editor,
	parseParams,
	show,
	parseSpec
	ucFirst,
	patchJQuery,
	usingPromise,
	delay
*/

const params = parseParams(location.search);

$(start);

async function start() {
	try {
		switch (params.action) {
			case undefined:
				showMain();
				break;

			case 'manualTest':
				await startManualTest();
				break;

			case 'automaticTest':
				await startAutomaticTest();
				break;

			default:
				throw new Error(`Unknown action '${params.action}'`);
		}
	} catch (error) {
		console.error(error);
		if (error.stack) {
			showError(`${error}<pre>${error.stack}</pre>`);
		} else {
			showError(error);
		}
	}
}

function showMain() {
	show(
		'Translation editor',
		`
			Welcome to the translation editor! Here you can experiment, but it's impossible to mess anything up.
			<img src="images/favicon.png" style="height: 1.2em;" />
		`
	);
}

/**
 * @param {string} message
 */
function showError(message) {
	show('Error', `<div style="white-space:pre-wrap">${message}</div>`);
}

function parseParamWikiAndTitle() {
	const { wiki, wikitext } = params;

	if (!wiki || !wikitext) {
		throw new Error("Parameters 'wiki' and 'wikitext' are required");
	}

	const [match, lang, title] = /^([a-z]+):(.+)$/.exec(wikitext) || [];
	if (!match) {
		throw new Error("Parameter 'wikitext' must be on the format 'lang:page'");
	}

	if (lang !== 'fr') {
		throw new Error('Only pages on fr.wiktionary.org are currently supported');
	}

	return { wiki, lang, title };
}

/**
 * @param {string} wiki
 */
function loadDefaultScripts(wiki) {
	return $.getScript(
		`https://${wiki}.wiktionary.org/w/load.php?debug=false&lang=${wiki}&modules=startup&only=scripts&skin=vector`
	);
}

async function startManualTest() {
	const { wiki, title } = parseParamWikiAndTitle();

	show(title, '<i>Loading...</i>');

	const titleEncoded = encodeURIComponent(title);
	const [, meta, html] = await Promise.all([
		loadDefaultScripts(wiki),

		// Get info about the page.
		$.getJSON(`/api/rest_v1/page/title/${titleEncoded}`),
		$.get(`/api/rest_v1/page/html/${titleEncoded}`)
	]);

	mw.config.set({
		wgPageName: meta.items[0].title,
		wgTitle: meta.items[0].title.replace(/^\w+:/, ''), // TODO Remove wgTitle, it shouldn't be used
		wgArticleId: meta.items[0].page_id,
		wgRevisionId: meta.items[0].rev,
		wgCurRevisionId: meta.items[0].rev
	});

	const parsedHtml = $($.parseHTML(html))
		// JQuery's parse function works as if the HTML is inserted directly into a <body>.
		// Thus, it removes <html>, <head> and <body>, but not their contents.
		// Filter out elements that would be in <head>.
		.filter(':not(meta, title, link, base)');

	show(title, parsedHtml);

	await loadGadget();
	monkeyPatchGadget();
}

async function loadGadget() {
	if (loadGadget.didLoad) {
		return;
	}
	loadGadget.didLoad = true;

	// The gadget requires the basic MediaWiki functions to be loaded. Wait
	// until they are.
	for (let i = 30; !mw.loader.using || !mw.hook; i *= 2) {
		await delay(i);
	}

	mw.loader.load('/gadget/translation-editor.css', 'text/css');

	await Promise.all([
		$.getScript('/gadget/silent-fail-storage.js'),
		$.getScript('/gadget/editor.js')
	]);

	await $.getScript('/gadget/translation-editor.js');
	// Remove all local storage items to allow easy testing
	localStorage.clear();
}

function monkeyPatchGadget() {
	const wikipageContentHookCheckbox = $('<input>', { type: 'checkbox' });
	const code = $('<textarea>', { rows: 20, readonly: true });
	$('#debug')
		.show()
		.append(
			$('<p>').append(
				$('<label>').append(
					wikipageContentHookCheckbox,
					' Highlight wikipage.content hook'
				)
			),
			code
		);

	mw.hook('wikipage.content').add(async elem => {
		if (wikipageContentHookCheckbox.prop('checked')) {
			elem.addClass('debug-highlight');
		}
	});

	// TODO Save, undo, undoAll and redo aren't currently monkey-patchable, so
	// let's also update the wikitext upon click anywhere on the page. We can
	// remove this hack one day.
	$(window).on('click', updateWikitext);
	updateWikitext();

	['edit', 'save', 'undo', 'undoAll', 'redo'].forEach(funcName => {
		const origFunc = editor[funcName];
		if (typeof origFunc !== 'function') {
			throw new Error(
				`Cannot monkey patch editor.${funcName}, because it isn't a function`
			);
		}

		editor[funcName] = (...args) => {
			origFunc.apply(editor, args);
			updateWikitext();
		};
	});

	async function updateWikitext() {
		code.val(await editor.wikitext());
	}
}

async function startAutomaticTest() {
	const { wiki, title } = parseParamWikiAndTitle();

	show(title, '<i>Loading...</i>');

	await loadDefaultScripts(wiki);
	await usingPromise(['jquery']);
	patchJQuery();

	const titleEncoded = encodeURIComponent(title);
	const specs = parseSpec(
		await $.get(`/w/index.php?action=raw&title=${titleEncoded}`)
	);

	// Make DOM.
	const dom = specs
		.map(spec => {
			const elem = $('<h1>').append(makeTestStatus(), spec.title);
			spec.elem = elem;

			return [
				elem,
				spec.scenarios.map(scenario => {
					const elem = $('<h2>').append(makeTestStatus(), scenario.title);
					scenario.elem = elem;

					return [
						elem,
						scenario.statements.map(statement =>
							statement.body.map((statementBody, i) => {
								let opts;
								if (statementBody.text.indexOf('\n') === -1) {
									opts = { text: statementBody.text };
								} else {
									opts = {
										text: statementBody.text.split('\n')[0] + ' [...]',
										title: statementBody.text
									};
								}

								const elem = $('<div>').append(
									makeTestStatus(),
									$('<b>', {
										text: i ? 'And' : ucFirst(statement.type)
									}),
									' ',
									$('<span>', opts)
								);

								if (statementBody.type === 'ERROR') {
									setStatus(elem, 'fail');
								}

								statementBody.elem = elem;

								return elem;
							})
						)
					];
				})
			];
		})
		.flat(5);

	const contentSandbox = $('<div>', { class: 'col-6' });

	show(
		title,
		$('<div>', { class: 'row' }).append(
			$('<div>', { class: 'col-6' }).append(dom),
			contentSandbox
		)
	);

	// Run tests.
	const context = { parent: contentSandbox };
	specLoop: for (const spec of specs) {
		setStatus(spec.elem, 'inProgress');
		let hasSpecFailure = false;

		scenarioLoop: for (const scenario of spec.scenarios) {
			setStatus(scenario.elem, 'inProgress');

			// Clean up from the previous scenario.
			contentSandbox.text('');
			try {
				editor.undoAll();
			} catch (e) {
				// It can throw if the editor was never initialized.
			}

			for (const statement of scenario.statements) {
				for (const statementBody of statement.body) {
					setStatus(statementBody.elem, 'wait');
					await $.ajaxComplete();
					setStatus(statementBody.elem, 'inProgress');

					try {
						await statementBody.exec(statementBody.params, context);
					} catch (error) {
						$('<div>', {
							class: 'error-message',
							text: error.message || error,
							title: error.stack
						}).appendTo(statementBody.elem);

						setStatus(statementBody.elem, 'fail');
						setStatus(scenario.elem, 'fail');
						setStatus(spec.elem, 'fail');
						hasSpecFailure = true;
						if (params.continueOnFailure) {
							continue scenarioLoop;
						} else {
							break specLoop;
						}
					}

					setStatus(statementBody.elem, 'ok');
				}
			}

			setStatus(scenario.elem, 'ok');
		}

		if (!hasSpecFailure) {
			setStatus(spec.elem, 'ok');
		}
	}
}

const statusSymbols = {
	ok: '✅',
	fail: '❌',
	notRun: '*️',
	inProgress: '🔄',
	wait: '⏳'
};

function makeTestStatus() {
	return $('<span>', { text: statusSymbols.notRun, class: 'status-icon' });
}

/**
 *
 * @param {JQuery<any>} parent
 * @param {'ok'|'fail'|'notRun'|'inProgress'|'wait'} status
 */
function setStatus(parent, status) {
	$(parent)
		.find('.status-icon')
		.text(statusSymbols[status]);
}
