/* exported parseSpec */
/* global
	loadGadget,
	mw,
	editor,
	$,
	setFieldValue,
	blurElem,
	delay,
	assertEqual,
	waitUntilNextMethodCall
*/

/**
 * @param {string} str
 * @param {RegExp} regex Must have exactly one capture.
 */
function getSectionsWithOneCapture(str, regex) {
	const parts = str.split(regex);
	const sections = [];
	for (let i = 1; i < parts.length; i += 2) {
		sections.push({
			head: parts[i],
			body: parts[i + 1]
		});
	}

	return sections;
}

/**
 * @returns {JQuery<any>}
 */
function getTranslationFormFromContext(context) {
	const { translationForm } = context;
	if (!translationForm) {
		throw new Error('Expected a translation form to be selected');
	}

	return translationForm;
}

const specStatements = {
	givenPage: {
		regex: /^the page "([^"]+)"$/,
		exec: (params, context) => {
			context.title = params[1];
			mw.config.set({
				wgPageName: params[1],
				// TODO wgTitle should not be used.
				wgTitle: params[1]
			});
		}
	},

	givenWikitext: {
		regex: /^the wikitext\s+<pre>([\s\S]*?)<\/pre>$/,
		exec: async (match, context) => {
			if (!context.title) {
				throw new Error('Expected page title to be set');
			}

			context.wikitext = match[1].trim();

			const [, data] = await Promise.all([
				loadGadget(),
				new mw.Api().get({
					action: 'parse',
					text: context.wikitext,
					title: context.title
				})
			]);

			editor.reinitWithWikitext(context.wikitext);

			const html = data.parse.text['*'];

			const elem = $(context.parent)
				.text('')
				.append($.parseHTML(html));

			mw.hook('wikipage.content').fire(elem);
		}
	},

	whenUsingTranslationForm: {
		regex: /^using translation form (\d+)$/,
		exec: (params, context) => {
			const num = +params[1];
			const form = $($(context.parent).find('.ed-lang-name')[num - 1]).parents(
				'form'
			);
			if (!form.length) {
				throw new Error(`Could not find translation form ${num}`);
			}

			context.translationForm = form;
		}
	},

	whenIEnterLanguage: {
		regex: /^I enter language "([^"\n]+)"$/,
		exec: async (params, context) => {
			await setFieldValue(
				getTranslationFormFromContext(context).find('.ed-lang-name'),
				params[1]
			);
		}
	},

	whenILeaveTheLanguageField: {
		regex: /^I leave the language field$/,
		exec: (_params, context) => {
			blurElem(getTranslationFormFromContext(context).find('.ed-lang-name'));
		}
	},

	whenIEnterWord: {
		regex: /^I enter word "([^"\n]+)"$/,
		exec: async (params, context) => {
			await setFieldValue(
				getTranslationFormFromContext(context).find('.ed-word'),
				params[1]
			);
		}
	},

	whenISubmitTheTranslationFrom: {
		regex: /^I submit the translation form$/,
		exec: async (params, context) => {
			getTranslationFormFromContext(context).trigger('submit');
			await waitUntilNextMethodCall(editor, 'edit');
		}
	},

	whenIClickTheSubmissionButton: {
		regex: /^I click the submission button$/,
		exec: async (params, context) => {
			getTranslationFormFromContext(context).trigger('submit');
		}
	},

	whenISelectAutocompletion: {
		regex: /^I select autocompletion "([^"\n]+)"$/,
		exec: params => {
			$('.ui-autocomplete a').each(function() {
				if (this.textContent === params[1]) {
					$(this).click();
				}
			});
		}
	},

	whenISelectGender: {
		regex: /^I select gender "([^"\n]+)"$/,
		exec: (params, context) => {
			getTranslationFormFromContext(context)
				.find('.ed-gender label')
				.each(function() {
					if (this.textContent === params[1]) {
						$(this).click();
					}
				});
		}
	},

	whenIClickTheMoreButton: {
		regex: /^I click the "More" button$/,
		exec: (params, context) => {
			getTranslationFormFromContext(context)
				.find('.ed-more')
				.click();
		}
	},

	whenIClickOnReportABug: {
		regex: /^I click on "Report a bug"$/,
		exec: (params, context) => {
			getTranslationFormFromContext(context)
				.find('.ed-feedback')
				.click();
			// TODO TypeError: "mw.config.get(...) is undefined"
		}
	},

	whenITypeInTheHeaderField: {
		regex: /^I type in the header field "([^\n]+)"$/,
		exec: params => {
			$('.oo-ui-fieldsetLayout-group input.oo-ui-inputWidget-input').val(
				params[1]
			);
		}
	},

	whenITypeInTheMessageField: {
		regex: /^I type in the message field "([^\n]+)"$/,
		exec: params => {
			$('.oo-ui-fieldsetLayout-group textarea.oo-ui-inputWidget-input').val(
				params[1]
			);
		}
	},

	whenIConfirmTheWordIsCapitalized: {
		regex: /^I confirm the word is capitalized$/,
		exec: (params, context) => {
			getTranslationFormFromContext(context)
				.find('.ed-error-case')
				.click();
		}
	},

	whenIConfirmIWantHuToBeUsed: {
		regex: /^I confirm I want "([^"\n]+)" to be used$/,
		exec: (params, context) => {
			getTranslationFormFromContext(context)
				.find('.ed-change-lang')
				.click();
		}
	},

	thenTheCurrentLanguageShouldBe: {
		regex: /^the current language should be "([^"\n]+)"$/,
		exec: (params, context) => {
			assertEqual(
				getTranslationFormFromContext(context)
					.find('.ed-lang-name')
					.val(),
				params[1]
			);
		}
	},

	thenTheCurrentWikitextShouldBe: {
		regex: /^the current wikitext should be\s+<pre>([\s\S]*?)<\/pre>$/,
		exec: async params => {
			assertEqual(await editor.wikitext(), params[1].trim());
		}
	},

	thenAutocompletionShouldSuggest: {
		regex: /^autocompletion should suggest "([^"\n]+)"$/,
		exec: async params => {
			// Default autocomplete delay is 300 ms.
			await delay(315);
			const autocompleted = $.map(
				$('.ui-autocomplete a'),
				elem => elem.textContent
			);
			const isIncluded = autocompleted.indexOf(params[1]) !== -1;

			assertEqual(isIncluded, true, `Got ${autocompleted.join(', ')}`);
		}
	},

	thenIShouldBeAskedWhetherTheWordIsActuallyCapitalized: {
		regex: /^I should be asked whether the word is actually capitalized$/,
		exec: (params, context) => {
			assertEqual(
				getTranslationFormFromContext(context).find('div.ed-errors').length,
				1
			);
		}
	},

	thenIShouldBeAskedWhetherIPrefer: {
		regex: /^I should be asked whether I prefer/,
		exec: (params, context) => {
			assertEqual(
				getTranslationFormFromContext(context).find('.ed-info-msg').length,
				1
			);
		}
	},

	thenTheFeedbackGUIShouldAppear: {
		regex: /^the feedback GUI should appear$/,
		exec: async () => {
			await mw.loader.using('mediawiki.feedback');
			assertEqual($('.oo-ui-fieldsetLayout-group').length, 1);
		}
	}

	/* TODO Find a way to check feedback submission is correct
	whenISubmitTheFeedbackForm: {
	    regex: /^I submit the feedback form$/,
	    exec: params => {
	        $('.oo-ui-processDialog-actions-primary .oo-ui-buttonElement-button')
	        .click();
	    }
	},

	thenTheSubmittedWikitextShouldBe: {
	    regex: /^the submitted wikitext should be\s+<pre>([\s\S]*?)<\/pre>$/,
	    exec: params => {
	        assertEqual(checkWikitextSentToFeedbackPage(), params[1]); // TODO
	    }
	}
	*/
};

/**
 * @param {string} statement
 */
function parseSpecStatement(statement) {
	statement = statement.trim();

	for (const type in specStatements) {
		const { regex, exec } = specStatements[type];
		const params = regex.exec(statement);
		if (params) {
			return {
				type,
				params,
				elem: undefined,
				text: statement,
				exec
			};
		}
	}

	return {
		type: 'ERROR',
		params: undefined,
		elem: undefined,
		text: `UNKNOWN STATEMENT: '${statement}'`,
		exec: () => {
			throw new Error('Unknown statement');
		}
	};
}

/**
 * @param {string} spec
 */
function parseSpec(spec) {
	const allErrors = [];

	const stories = getSectionsWithOneCapture(
		spec,
		/== Story: ([^\n=]+) ==\n+/
	).map(({ head, body }) => {
		return {
			type: 'story',
			title: head,
			elem: undefined,
			scenarios: getSectionsWithOneCapture(
				body,
				/=== Scenario: ([^\n=]+) ===\n+/
			).map(({ head, body }) => {
				return {
					type: 'scenario',
					title: head,
					statements: getSectionsWithOneCapture(
						body,
						/: '''(Given|When|Then)''' /
					).map(({ head, body }) => {
						return {
							type: head.toLowerCase(),
							body: body.split("\n: '''And''' ").map(x => {
								try {
									return parseSpecStatement(x);
								} catch (e) {
									allErrors.push(e ? e.message : e);
								}
							})
						};
					})
				};
			})
		};
	});

	if (allErrors.length) {
		console.error(allErrors);
		throw new Error(allErrors.join('\n'));
	}

	return stories;
}
