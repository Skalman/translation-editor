/* exported
	parseParams,
	show,
	delay,
	setFieldValue,
	blurElem,
	assertEqual,
	ucFirst,
	patchJQuery,
	usingPromise,
	waitUntilNextMethodCall
*/
/* global $, mw */

/**
 * @param {string} url
 */
function parseParams(url) {
	/**
	 * @type {{[key: string]: string | undefined}}
	 */
	const params = {};

	url.replace(/[&?]([^=#]+)=([^&#]*)/g, (_match, key, value) => {
		key = decodeURIComponent(key);
		value = decodeURIComponent(value);
		params[key] = value;
	});
	return params;
}

/**
 * @param {string} title
 * @param {string|JQuery<any>|JQuery<any>[]} content
 */
function show(title, content) {
	document.title = `${title} - Translation editor`;
	$('#firstHeading').text(title);
	const parent = $('#mw-content-text').text('');

	if (Array.isArray(content)) {
		parent.append(...content);
	} else {
		parent.append(content);
	}

	if (window.mw && mw.hook) {
		mw.hook('wikipage.content').fire(parent);
	}
}

/**
 * @param {number|undefined} timeout
 * @returns {Promise<void>}
 */
function delay(timeout) {
	return new Promise(resolve => setTimeout(resolve, timeout));
}

/**
 * @param {JQuery<any>} field
 * @param {string|number} value
 */
async function setFieldValue(field, value) {
	if (field.length !== 1) {
		console.error(field, value);
		throw new Error(
			`Expected to set value of a single field, but got ${field.length} fields`
		);
	}

	field.trigger('focus');
	const isBrowserFocusEnabled = document.activeElement !== field[0];
	if (!isBrowserFocusEnabled) {
		field.triggerHandler('focus');
	}
	await delay(50);
	field.val(value);
	field.trigger('keydown');
	field.trigger('change');
	await delay(50);
}

/**
 * @param {JQuery<any>} elem
 */
function blurElem(elem) {
	const isElemFocused = document.activeElement !== elem[0];
	if (isElemFocused) {
		elem.trigger('blur');
	} else {
		elem.triggerHandler('blur');
	}
}

function assertEqual(actual, expected, message = undefined) {
	if (
		actual === expected ||
		JSON.stringify(actual) === JSON.stringify(expected)
	) {
		return;
	}

	console.error('Expected', expected, 'but got', actual);

	throw new Error(
		[
			message,
			'Expected:',
			JSON.stringify(expected),
			'But got:',
			JSON.stringify(actual)
		]
			.filter(x => x)
			.join('\n')
	);
}

/**
 * @param {string} str
 */
function ucFirst(str) {
	return str[0].toUpperCase() + str.substr(1);
}

function patchJQuery() {
	if ($.isPatched) {
		return;
	}
	$.isPatched = true;
	let numCurrentJQueryAjaxCalls = 0;
	let ajaxCompleteCallbacks = [];

	// While testing, we cannot make requests to other servers. Instead they must be proxied.
	const _ajax = $.ajax;
	$.ajax = function(options, ...args) {
		let url = options.url || '';
		if (/^(https?:)?\/\//.test(url) && options.dataType !== 'script') {
			options = { ...options };

			// URLs starting with '//' not supported.
			if (url.startsWith('//')) {
				url = `https:${url}`;
			}

			// Don't send origin query parameter, if any.
			url = url.replace(/([?&])origin=[^&]+&?/, '$1').replace(/[?&]$|/, '');

			// GET data must be included directly in the URL.
			if ((options.method === 'GET' || !options.method) && options.data) {
				url += (url.includes('?') ? '&' : '?') + $.param(options.data);
				options.data = undefined;
			}

			options.url = '/proxy?url=' + encodeURIComponent(url);
		}

		const ajaxResult = _ajax.call(this, options, ...args);
		numCurrentJQueryAjaxCalls++;

		if (ajaxResult.then) {
			ajaxResult.then(completeAjaxCall, completeAjaxCall);
		}

		return ajaxResult;
	};

	/**
	 * @returns Promise which resolves when there are no more in-flight AJAX requests.
	 */
	$.ajaxComplete = () => {
		if (!numCurrentJQueryAjaxCalls) {
			return Promise.resolve();
		} else {
			return new Promise(resolve => {
				ajaxCompleteCallbacks.push(resolve);
			});
		}
	};

	async function completeAjaxCall() {
		numCurrentJQueryAjaxCalls--;

		if (!numCurrentJQueryAjaxCalls && ajaxCompleteCallbacks.length) {
			// Allow other callbacks to be run first.
			await delay();

			// If a new ajax call was started in another callback, don't run these callbacks yet.
			if (!numCurrentJQueryAjaxCalls && ajaxCompleteCallbacks.length) {
				const toCall = ajaxCompleteCallbacks;

				// eslint-disable-next-line require-atomic-updates
				ajaxCompleteCallbacks = [];
				toCall.forEach(x => x());
			}
		}
	}
}

/**
 * @param {string[]} dependencies
 */
async function usingPromise(dependencies) {
	while (!mw.loader.using) {
		await delay(100);
	}

	return new Promise(resolve => {
		mw.loader.using(dependencies, resolve);
	});
}

/**
 * Resolves the promise when a method is called. The method will be temporarily
 * replaced.
 * @param {object} object
 * @param {string} methodName
 */
function waitUntilNextMethodCall(object, methodName) {
	return new Promise(resolve => {
		const origMethod = object[methodName];
		object[methodName] = function(...args) {
			// Restore the original method.
			object[methodName] = origMethod;

			try {
				return origMethod.apply(this, args);
			} finally {
				// Regardless of whether the method throws an exception, we want to resolve the promise.
				resolve(true);
			}
		};
	});
}
