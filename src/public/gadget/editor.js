/**
 * Editor inspired by [[en:User:Conrad.Irwin]]'s editor.js.
 * It aims to be generic, usable also by other scripts than the Translation editor.
 * https://en.wiktionary.org/wiki/User:Conrad.Irwin/editor.js
 */

// Singleton editor
window.editor = (function() {
	/* global $, mw, silentFailStorage */

	if (mw.config.get('wgRevisionId') !== mw.config.get('wgCurRevisionId')) {
		return {};
	}

	// private variables and functions
	var ed = { enabled: true },
		api,
		elem,
		history,
		// Points at the action that has been carried out
		cursor;

	function updateDisabled() {
		elem.find('.ed-undo').prop('disabled', cursor === 0);
		elem.find('.ed-redo').prop('disabled', cursor === history.length - 1);
		elem.find('.ed-save').prop('disabled', history[cursor].save !== 'yes');
	}

	function show() {
		elem.show();
		if (elem.hasClass('ed-highlight')) {
			setTimeout(function() {
				elem.removeClass('ed-highlight');
				if (!silentFailStorage.getItem('ed-noinfo')) {
					elem.find('.ed-info').show();
				}
			}, 500);
		}
	}

	// public methods
	ed.edit = edit;
	function edit(rev) {
		init();
		history.length = cursor + 1;
		if (!rev.save) {
			rev.save = 'yes';
		} else if (rev.save === 'ifprev') {
			rev.save = history[cursor].save;
		}
		history.push(rev);
		redo();
		show();
	}

	ed.undo = undo;
	function undo() {
		history[cursor].undo();
		cursor--;
		updateDisabled();
	}

	ed.undoAll = undoAll;
	function undoAll() {
		while (cursor) {
			undo();
		}
		elem.hide();
	}

	ed.redo = redo;
	function redo() {
		history[cursor + 1].redo();
		cursor++;
		updateDisabled();
	}

	ed.save = save;
	function save() {
		var wikitext = history[cursor].wikitext;

		// Allow callbacks to make last-minute modifications before saving
		for (var i = cursor; i; i--) {
			if (history[i].onsave) {
				wikitext = history[i].onsave(wikitext);
			}
		}

		elem.find('.ed-info').hide();
		var log = $('<div>', { class: 'ed-save' })
			.append(
				$('<small>', { text: summary() }),
				' ',
				$('<b>', { text: 'Sauvegarde en cours...' })
			)
			.appendTo(elem.find('.ed-inner'));

		api
			.post({
				action: 'edit',
				title: mw.config.get('wgPageName'),
				text: wikitext,
				summary: summary(true),
				notminor: '',
				token: mw.user.tokens.get('editToken')
			})
			.done(function(data) {
				if (!data.edit || data.edit.result !== 'Success') {
					log
						.addClass('error')
						.find('b')
						.text("Impossible d'enregistrer");
					return;
				}

				$('.ed-added').removeClass('ed-added');

				history.length = 1;
				cursor = 0;
				log.find('b').text('Enregistré');
				log.append(
					' ',
					$('<a>', {
						text: 'Voir les changements',
						href:
							mw.config.get('wgScript') +
							'?title=' +
							encodeURIComponent(mw.config.get('wgPageName')) +
							'&diff=' +
							data.edit.newrevid +
							'&oldid=' +
							data.edit.oldrevid
					})
				);
				history[0].wikitext = wikitext;
				updateDisabled();
			})
			.fail(function(error) {
				log
					.find('b')
					.addClass('error')
					.text('Sauvegarde impossible');
				console.error(error);
			});
	}

	ed.wikitext = wikitext;
	function wikitext() {
		return (
			init() || new $.Deferred().resolve(history[cursor].wikitext).promise()
		);
	}

	ed.summary = summary;
	function summary(addAssisted) {
		var parts = {},
			// In case the editor is used for something other than translations,
			// 'Traductions :' is display if only translations have been edited.
			onlyTranslations = true;
		for (var i = 1; i <= cursor; i++) {
			var h = history[i];
			if (!h.summary) {
				onlyTranslations = false;
				continue;
			}
			if (h.summaryType) {
				if (!/gloss/.test(h.summaryType) && h.summaryType !== 'transl') {
					onlyTranslations = false;
				}
				if (parts[h.summaryType] === undefined) {
					parts[h.summaryType] = h.summary;
				} else {
					parts[h.summaryType] += ' ; ' + h.summary;
				}
			} else if (!parts._) {
				onlyTranslations = false;
				parts._ = h.summary;
			} else {
				parts._ += ' ; ' + h.summary;
			}
		}
		return (
			(onlyTranslations ? 'Traductions : ' : '') +
			$.map(parts, function(x) {
				return x;
			}).join(' ; ') +
			(addAssisted ? ' (assisté)' : '')
		);
	}

	ed.init = init;
	function init() {
		if (elem) {
			return;
		}
		// Warn before leaving the page if there are unsaved changes.
		$(window).on('beforeunload', function() {
			if (cursor) {
				return 'Vous avez des modifications non enregistrées.';
			}
		});
		history = [
			{
				redo: null,
				undo: null,
				wikitext: null,
				save: 'no'
			}
		];
		cursor = 0;

		// Experimental: info alert explaining that multiple edits are possible
		var infoAlert = $('<div>', {
			class: 'ed-info',
			html:
				"Vous pouvez continuer d'ajouter des traductions. " +
				"Quand vous aurez réalisé toutes les modifications désirées, merci d'enregistrer." +
				'<span class="ed-info-leave">Ne plus afficher ce message</span>' +
				'<span class="ed-info-close">×</span>'
		});
		infoAlert.find('.ed-info-close').click(function() {
			infoAlert.fadeOut();
		});
		infoAlert.find('.ed-info-leave').click(function() {
			silentFailStorage.setItem('ed-noinfo', 'true');
			infoAlert.fadeOut();
		});

		elem = $('<div>', { class: 'ed-box ed-highlight' })
			.append(
				'<a class="ed-close" href="#">&times;</a>' +
					'<div class="ed-inner">' +
					'<button class="ed-save" accesskey="s">Enregistrer les modifications</button><br>' +
					'<button class="ed-undo">Annuler</button>' +
					'<button class="ed-redo" disabled>Rétablir</button>' +
					'</div>'
			)
			.append(infoAlert);
		elem.find('.ed-close').click(function(e) {
			undoAll();
			e.preventDefault();
		});
		elem.find('.ed-save').click(save);
		elem.find('.ed-undo').click(undo);
		elem.find('.ed-redo').click(redo);
		elem.appendTo('body');

		api = new mw.Api();
		var promise;
		if (initialWikitext) {
			promise = new $.Deferred().resolve(initialWikitext).promise();
		} else {
			promise = api
				.get({
					action: 'query',
					prop: 'revisions',
					titles: mw.config.get('wgPageName'),
					rvprop: 'content'
				})
				.then(function(data) {
					return data
						.query.pages[mw.config.get('wgArticleId')].revisions[0]['*'];
				});
		}

		return promise.then(function(wikitext) {
			// Some scripts are strict, so remove trailing whitespace on non-empty lines
			wikitext = wikitext.replace(/(\S)[ \t]+(\n|$)/g, '$1$2');

			history[0].wikitext = wikitext;
			return wikitext;
		});
	}

	var initialWikitext;
	ed.reinitWithWikitext = reinitWithWikitext;
	function reinitWithWikitext(wikitext) {
		initialWikitext = wikitext;
		elem = undefined;
		init();
	}

	return ed;
})();
