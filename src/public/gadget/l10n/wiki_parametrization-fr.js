if (typeof TranslationEditor !== 'undefined') {
	window.TranslationEditor.wikiParametrization = {
		conf: {
			hasTraditionalCharactersField: true
		},
		// Page where users feedbacks are published
		feedbackDestPage: 'Discussion_MediaWiki:Gadget-translation_editor.js',
		// Default object title for Feedback form. <pagename> is replaced by the actual page name
		feedbackFormDefaultHeader: 'Traductions dans "<pagename>"',
		feedbackFormMessagePlaceholder: 'Veuillez entrer votre message ici',

		// Name of the pseudo-language for translingual entries.
		// Useful if your wiki makes translingual translations (like species names) come first
		translingualName: 'conventions internationales',

		// Text to ignore in translations list (eg. fr.wikt's {{ébauche-trad}} template)
		itemsToIgnore: ['Traductions manquantes.'],

		translation: {
			transTopTemplate: 'trad-début',
			transBottomTemplate: 'trad-fin',
			// Format of top translation template with a gloss
			topTransTemplateWithGloss: '{{trad-début|<gloss>}}',
			// Error message displayed in the browser console when a top translation template isn't found in the wikitext
			// <index> will be automatically replaced by the actual index
			errorNthTransTopTemplateNotFound:
				'TRANSLATION EDITOR: the <index>-th {{<transTopTemplate>}} was not found in wikitext.',

			transTemplateParameters: {
				trans: 'tr=',
				tradi: 'tradi=',
				displayed: 'dif='
			},
			// Format used for new translations.
			// <lang code> stands for language code
			// <lang name> for language name
			// <word> for the formatted word (it'll be replaced eg by)
			newTranslationFormat: '* {{T|<lang code>}} : <word>',

			// regular expression matching the language part of a translation line in wikicode
			// FR.WIKT SPECIFIC COMMENT:
			// The following regex contains [(:], because it's possible to have
			// a variant specified after the main language name.
			regexMatchTranslation: '\\*\\s*{{T\\|([^}|]+?)(?:\\|trier)?}}\\s*[(:]'
		}
	};
}
