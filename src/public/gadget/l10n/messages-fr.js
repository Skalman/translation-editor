if (typeof TranslationEditor !== 'undefined') {
	window.TranslationEditor.messages = {
		colon: ' :', // check whether you want a space before colon or not

		glossTooltip: 'Changer la description',
		gloss: 'Définition:',
		preview: 'Prévisualiser',
		loading: 'Chargement',
		glossUnchanged: "La description n'a pas changé",
		glossInvalid:
			"La description n'a pas un format correct : elle" +
			' contient du wikitexte ([]{}#|) (alors que {{<transTopTemplate>}} est absent)',

		editSummaryHeader: 'en-tête :',

		translitterationLabel: 'Translittération :',
		translitterationPlaceholder: 'ex : khimera pour химера',
		traditionalChineseCharactersLabel: 'Écriture traditionnelle :',
		traditionalChineseCharactersPlaceholder: 'ex : 軍團 pour 군단',
		pagenameLabel: 'Nom de page',
		pagenameTooltip:
			'Si la traduction ne correspond pas à un nom de page valide sur le ' +
			'Wiktionnaire, il est possible de préciser le nom de page à utiliser ici ' +
			'(le lien sur la traduction visera alors cette page)',
		pagenamePlaceholder: 'ex : amo pour amō',

		addTranslation: 'Ajouter une traduction en ',
		langNameTooltip: 'Nom de langue (anglais,…)',
		langNamePlaceholder: 'langue à choisir',
		translationTooltip: 'traduction',
		translationPlaceholder: 'traduction',
		addButton: 'Ajouter',
		moreButton: 'Plus',
		lessButton: 'Moins',
		feedbackButton: 'Signaler une anomalie – Suggérer une amélioration',
		// if a language name - eg. hu in French - is more often used as a language code - Hungarese -, then
		// it is automatically changed by the more popular language name (Hungarese),
		// while the following message is displayed to the user so it can change the behavior if needed:
		// (The following tags will automagically be replaced:
		// * <targetLangName> → by the target language name (eg. Hungarese),
		// * <initialLangName> → replaced by the initial user input,
		// * <click> and </click> must surround the text that can be clicked to cancel the replacement
		langNameReplacedByACodeMessage:
			'Il a été supposé que vous voulez ajouter une traduction en <preferredLangName>.<br>' +
			'Si vous vouliez en fait ajouter une traduction en <otherLangName>, <click>cliquer ici</click>.',
		// Same, after user prefs were saved
		savedPreferredLanguage:
			'"<preferredLangName>" a été choisi plutôt que "<otherLangName>". <click>Annuler</click>.',

		errorMessageComma: 'une virgule',
		errorMessageSemicolon: 'un point-virgule',
		errorMessageSlash: 'une barre oblique',
		errorLangUndefined: "La langue « <lang> » n'est pas définie.",
		errorLangMissing: 'Entrez le nom de langue (anglais, suédois,…)',
		errorTranslationMissing: 'Entrez la traduction',
		errorTranslationIntoWikiLanguageName:
			"Il n'est pas possible d'ajouter une traduction en français. " +
			'À la place, veuillez utiliser la section « Synonymes » ou ' +
			'« Variantes dialectales ».',
		warningTranslationContainsUncommonCharacter:
			'Êtes-vous certain que la traduction contient <character> ? ' +
			"Si tel n'est pas le cas, <br>veuillez insérer les traductions une par une, " +
			'en cliquant sur le bouton « Ajouter » après chaque insertion de traduction.<br>' +
			'Si vous êtes sûr de votre coup, veuillez <click>cliquer ici</click>.',
		warningTranslationIsCapitalized:
			'Êtes-vous certain que la majuscule fait partie de la traduction ?<br>' +
			"Si tel n'est pas le cas, merci de corriger cela.<br>" +
			'Sinon, veuillez <click>cliquer ici</click>.',
		errorTranslationContainsWikitext:
			"La traduction n'est pas dans un format correct : " +
			'elle contient du wikitexte ([]{}|=)',
		errorHttpError:
			'Vous ne pouvez pas charger la traduction. Êtes-vous en ligne ?',

		// Name of the pseudo-language for translingual entries.
		// Useful if your wiki makes translingual translations (like species names) come first
		translingualName: 'conventions internationales',
		unmanagedTranslationsFormatting:
			"Il n'est pas possible d'ajouter une traduction dans cette langue," +
			' car le format est inhabituel.',

		translation: {
			errorNotificationHTMLCommentInHeader:
				'Le wikitexte contient un commentaire "<!--". ' +
				'La page doit être modifiée manuellement.',
			// The following tags will automagically be replaced:
			// * <transBottomTemplate> → by the {{trans-top}} local name, that is in the local parametrization file
			errorNotificationtransBottomMissing:
				'Il manque probablement un modèle {{<transBottomTemplate>}} dans le wikicode.',
			errorNotificationHTMLCommentInTranslations:
				'Le wikitexte contient un commentaire "<!--".' +
				'La page doit être modifiée manuellement.',

			// When a unusual format is detected for translations in a specific language
			errorWeirdFormat:
				"Il n'est pas possible d'ajouter une traduction" +
				' dans cette langue, car le format est inhabituel.'
		}
	};
}
