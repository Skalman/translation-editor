if (typeof TranslationEditor !== 'undefined') {
	window.TranslationEditor.messages = {
		colon: ':', // check whether you want a space before colon or not

		glossTooltip: 'Change description',
		gloss: 'Gloss:',
		preview: 'Preview',
		loading: 'Loading',
		glossUnchanged: "Gloss hasn't changed",
		glossInvalid:
			'Gloss has an incorrect format: it contains wikitext' +
			' ([]{}#|) (whereas {{<transTopTemplate>}} is absent)',

		editSummaryHeader: 'header:',

		translitterationLabel: 'Translitteration:',
		translitterationPlaceholder: 'ex: khimera for химера',
		traditionalChineseCharactersLabel: 'Traditional Chinese characters:',
		traditionalChineseCharactersPlaceholder: 'ex: 軍團 for 군단',
		pagenameLabel: 'Pagename',
		pagenameTooltip:
			'If the translation does not correspond to a valid pagename, ' +
			'it is possible to indicate the pagename to point to here',
		pagenamePlaceholder: 'ex: amo for amō',

		addTranslation: 'Add a translation in ',
		langNameTooltip: 'Language name (French,…)',
		langNamePlaceholder: 'language to choose',
		translationTooltip: 'translation',
		translationPlaceholder: 'translation',
		addButton: 'Add',
		moreButton: 'More',
		lessButton: 'Less',
		feedbackButton: 'Report a bug – Suggest a feature',
		// if a language name - eg. hu in French - is more often used as a language code - Hungarese -, then
		// it is automatically changed by the more popular language name (Hungarese),
		// while the following message is displayed to the user so it can change the behavior if needed:
		// (The following tags will automagically be replaced:
		// * <targetLangName> → by the target language name (eg. Hungarese),
		// * <initialLangName> → replaced by the initial user input,
		// * <click> and </click> must surround the text that can be clicked to cancel the replacement
		langNameReplacedByACodeMessage:
			'It has been assumedthat you wanted to add a translation in <preferredLangName>.<br>' +
			'If you actually wanted to add a translation in <otherLangName>, <click>click here</click>.',
		// Same, after user prefs were saved
		savedPreferredLanguage:
			'"<preferredLangName>" has been chosen instead of "<otherLangName>". <click>Cancel</click>.',

		errorMessageComma: 'a comma',
		errorMessageSemicolon: 'a semi-colon',
		errorMessageSlash: 'a slash',
		errorLangUndefined: 'Language "<lang>" is not defined.',
		errorLangMissing: 'Enter the language name (English, Swedish,…)',
		errorTranslationMissing: 'Enter the translation',
		errorTranslationIntoWikiLanguageName:
			'It is not possible to add a translation in English. ' +
			'Instead, please use the "Synonyms" paragraph.',
		warningTranslationContainsUncommonCharacter:
			'Are you certain the translation contains <character>? ' +
			'If this is not the case, <br>please insert translations one by one, ' +
			'by clicking the button "Add" after each translation insertion.<br>' +
			'If you know what you are doing, please <click>click here</click>.',
		warningTranslationIsCapitalized:
			'Are you certain the uppercase letter is justified?<br>' +
			'If this is not the case, please fix this.<br>' +
			'otherwise, please <click>click here</click>.',
		errorTranslationContainsWikitext:
			'Translation does not have a correct format: ' +
			'it contains wikitext ([]{}|=)',
		errorHttpError: 'Translation cannot be loaded. Are you online?',

		// Name of the pseudo-language for translingual entries.
		// Useful if your wiki makes translingual translations (like species names) come first
		translingualName: 'translingual',
		unmanagedTranslationsFormatting:
			'It is not possible to add a translation in this language,' +
			' because the format is unusual.',

		translation: {
			errorNotificationHTMLCommentInHeader:
				'The wikicode contains a comment "<!--". ' +
				'This page has to be modified manually.',
			// The following tags will automagically be replaced:
			// * <transBottomTemplate> → by the {{trans-top}} local name, that is in the local parametrization file
			errorNotificationtransBottomMissing:
				'A {{<transBottomTemplate>}} template is probably missing in the wikicode.',
			errorNotificationHTMLCommentInTranslations:
				'The wikicode contains a comment "<!--".' +
				'This page has to be modified manually.',

			// When a unusual format is detected for translations in a specific language
			errorWeirdFormat:
				'It is not possible to add a translation' +
				' in this language, because the format is unusual.'
		}
	};
}
