/**
 * Assistant for adding translations
 *
 * Inspired by [[en:User:Conrad.Irwin/editor.js]]
 */

/* global jQuery, mediaWiki, editor, silentFailStorage, wgPageName */

(function(mw, $) {
	'use strict';

	var // Localized UI texts
		messages,
		// wiki parameters
		parametrization,
		// Map of languages on the format { "language code": "language name" },
		// which will be loaded upon focusing the form.
		langMap,
		// Array of language names
		langNameArray,
		// Map code -> language name
		langCodeMap,
		// Map of code aliases
		langCodeAliasMap,
		// ID used by `addHeadingUpdater()` to correctly set the correct edit summaries.
		headingIdCounter = 0,
		// translation object, containing methods for handling translations (add translation/set gloss/etc.)
		translation;
	window.TranslationEditor = {};

	if (!editor.enabled || mw.config.get('wgAction') !== 'view') return;
	$.when(
		$.getScript('gadget/l10n/messages-fr.js'),
		$.getScript('gadget/l10n/wiki_parametrization-fr.js')
	).done(function() {
		messages = window.TranslationEditor.messages;
		parametrization = window.TranslationEditor.wikiParametrization;
		translation = getTranslationObject();

		// Initialize translations each time
		mw.hook('wikipage.content').add(function(parent) {
			parent.find('div.translations').each(function() {
				addTranslationForm(this);
				addHeadingUpdater(this);
			});

			// TODO FR.WIKT specific handling
			// Hide missing translations note, if there is one.
			parent.find('.trad-stub').hide();
			parent
				.find('.trad-stub')
				.parent('li')
				.hide();
		});
	});

	function getTranslationTableIndex(table) {
		return $.inArray(table, $('div.translations'));
	}

	function getErrorHtml(message) {
		return (
			'<img src="//upload.wikimedia.org/wikipedia/commons/4/4e/MW-Icon-AlertMark.png"> ' +
			message
		);
	}

	function getInfoHtml(message) {
		return (
			'<img src="//upload.wikimedia.org/wikipedia/commons/f/f8/Farm-Fresh_information.png"> ' +
			message
		);
	}

	function addHeadingUpdater(table) {
		var id = headingIdCounter++;

		var self = $(table)
			.parent()
			.parent('.NavContent')
			.prev('.NavHead');

		var editHead = $('<a>', {
			href: '#',
			text: '±',
			class: 'ed-edit-head',
			title: messages.glossTooltip
		}).prependTo(self);

		function removeGlossNodes() {
			var nodes = [];
			$.each(self[0].childNodes, function(i, node) {
				if (
					node.className !== 'ed-edit-head' &&
					node.className !== 'NavToggle'
				) {
					nodes.push(node);
				}
			});
			$(nodes).detach();
			return nodes;
		}

		var glossNodes;
		editHead.click(function(e) {
			e.preventDefault();
			if (self.find('form').length) {
				self.find('form').remove();
				self.append(glossNodes);
				return;
			}

			editHead.text(messages.loading + '…');

			editor.wikitext().then(function(wikitext) {
				editHead.text('±');
				glossNodes = removeGlossNodes();
				var prevGlossNodes = glossNodes;

				var gloss = translation.getGloss(
					wikitext,
					getTranslationTableIndex(table)
				);
				var glossToHandle = gloss.standard ? gloss.text : gloss.transTop;

				var form = $('<form>', {
					html:
						'<label>' +
						messages.gloss +
						' <input name="gloss"></label>' +
						'<button type="submit">' +
						messages.preview +
						'</button> ' +
						'<span class="ed-loading">' +
						messages.loading +
						'…</span>' +
						'<span class="ed-errors"></span>'
				});
				function error(msg) {
					form.find('.ed-errors').html(getErrorHtml(msg));
				}

				self.append(form);

				form
					.find('input')
					.val(glossToHandle)
					.focus();
				form
					.click(function(e) {
						e.stopPropagation();
					})
					.submit(function(e) {
						e.preventDefault();
						if (this.gloss.value === glossToHandle) {
							error(messages.glossUnchanged);
							return;
						}
						var glossWikitext = $.trim(this.gloss.value);
						if (
							!translation.isTransTop(glossWikitext) &&
							translation.containsWikitext(glossWikitext)
						) {
							error(
								messages.glossInvalid.replace(
									'<transTopTemplate>',
									parametrization.translation.transTopTemplate
								)
							);
							return;
						}
						form.find('.ed-loading').show();

						$.when(
							parseWikitext(translation.makeTransTop(glossWikitext)),

							// get wikitext again in case it has changed since last time
							editor.wikitext()
						).then(function(glossHtml, wikitext) {
							glossHtml = $(glossHtml);
							var prevClass = self.parent('.NavFrame').attr('class');
							var newClass = glossHtml.filter('.NavFrame').attr('class');

							glossHtml = glossHtml.find('.NavHead').contents();

							form.remove();
							wikitext = translation.setGloss(
								wikitext,
								getTranslationTableIndex(table),
								glossWikitext
							);
							editor.edit({
								wikitext: wikitext,
								summary:
									messages.editSummaryHeader + ' "' + glossWikitext + '"',
								summaryType: 'gloss' + id,
								redo: function() {
									removeGlossNodes();
									$('<span>', {
										class: 'ed-added',
										html: glossHtml
									}).appendTo(self);
									if (prevClass !== newClass) {
										self.parent('.NavFrame').attr('class', newClass);
									}
								},
								undo: function() {
									removeGlossNodes();
									self.append(prevGlossNodes);
									if (prevClass !== newClass) {
										self.parent('.NavFrame').attr('class', prevClass);
									}
								}
							});
						});
					});
			});
		});
	}

	function addTranslationForm(table) {
		var self = $(table),
			navContent = self.parent().parent();

		// If there aren't any translations yet, add the "<ul>"
		// in order to attach the form to it
		if (
			self.children().length === 0 ||
			self
				.children()
				.first()
				.prop('nodeName')
				.toLowerCase() !== 'ul'
		) {
			self.append($('<ul>'));
		}

		// Language options
		var langMeta = {
			'': '', // Default

			de: 'm f n p',
			ar: 'm f d p trans',
			an: 'm f p',
			hy: 'trans',
			xcl: 'trans',
			ast: 'm f n p',
			bn: 'trans',
			be: 'm f n p trans',
			my: 'trans',
			br: 'm f p',
			bg: 'm f n p trans',
			ca: 'm f p',
			zh: 'trans tradi',
			cop: 'm f trans',
			ko: 'trans tradi',
			co: 'm f p',
			da: 'c n p',
			dz: 'trans',
			myv: 'trans',
			es: 'm f p',
			fo: 'm f n',
			fur: 'm f p',
			ka: 'trans',
			gd: 'm f d p',
			ga: 'm f p',
			gl: 'm f p',
			cy: 'm f p',
			got: 'm f n p trans',
			gu: 'trans',
			el: 'trans m f n p',
			grc: 'trans m f n d p',
			gkm: 'trans',
			he: 'm f trans',
			hbo: 'm f trans',
			hi: 'm f trans',
			iu: 'trans',
			is: 'm f n p',
			it: 'm f mf p',
			ja: 'trans',
			kk: 'trans',
			km: 'trans',
			lo: 'trans',
			la: 'm f n p',
			lv: 'm f p',
			lij: 'm f p',
			lt: 'm f p',
			lb: 'm f n p',
			mk: 'm f n p trans',
			ml: 'trans',
			cmn: 'trans tradi',
			mr: 'trans',
			nan: 'tradi',
			mwl: 'm f p',
			mn: 'trans',
			nm: 'm f n p',
			ne: 'trans',
			nb: 'm f n p mfp',
			nn: 'm f n p mfp',
			os: 'trans',
			fa: 'trans',
			pms: 'm f p',
			plodarisch: 'm f n p',
			pl: 'm f n p',
			pt: 'm f p',
			rm: 'm f p',
			ro: 'm f n p',
			ru: 'm f n trans',
			sa: 'm f n d p trans',
			sc: 'm f p',
			sr: 'm f n p trans',
			xsr: 'trans',
			scn: 'm f p',
			sl: 'm f n',
			sv: 'c n',
			sk: 'm f n',
			ta: 'trans',
			tg: 'trans',
			tt: 'trans',
			th: 'trans',
			bo: 'trans',
			uk: 'm f n p trans',
			vec: 'm f p',
			sga: 'm f n d p',
			non: 'm f n p',
			cu: 'm f n d p',
			yi: 'm f n p trans'
		};

		var options =
			$.map(
				{
					gender: {
						m: 'masc.',
						f: 'fém.',
						mf: 'masc. & fém.',
						n: 'neutre',
						c: 'commun',
						s: 'singulier',
						d: 'duel',
						p: 'pluriel',
						mp: 'masc. plur.',
						fp: 'fém. plur.',
						np: 'neutre plur.',
						mfp: 'masc. & fém. plur.'
					}
				},
				function(items, name) {
					items = $.map(items, function(text, value) {
						return (
							'<label class="ed-' +
							value +
							'">' +
							'<input type="radio" name="' +
							name +
							'" value="' +
							value +
							'">' +
							text +
							'</label>'
						);
					});
					return (
						'<p class="ed-options ed-' + name + '">' + items.join(' ') + '</p>'
					);
				}
			).join('') +
			'<p class="ed-options"><label class="ed-trans">' +
			messages.translitterationLabel +
			' ' +
			'<input name="trans" placeholder="' +
			messages.translitterationPlaceholder +
			'"></label></p>' +
			(parametrization.conf.hasTraditionalCharactersField
				? '<p class="ed-options"><label class="ed-tradi">' +
				  messages.traditionalChineseCharactersLabel +
				  ' '
				: '') +
			'<input name="tradi" placeholder="' +
			messages.traditionalChineseCharactersPlaceholder +
			'"></label></p>' +
			'<p class="ed-options"><label class="ed-pagename">' +
			messages.pagenameLabel +
			'<span title="' +
			messages.pagenameTooltip +
			'">(?)</span> : ' +
			'<input name="pagename" placeholder="' +
			messages.pagenamePlaceholder +
			'"></label></p>';

		var form = $(
			$.parseHTML(
				'<form>' +
					'<p><label><span class="txt-label">' +
					messages.addTranslation +
					'</span>' +
					'<input class="ed-lang-name" name="langName" size="13" title="' +
					messages.langNameTooltip +
					'" placeholder="' +
					messages.langNamePlaceholder +
					'"></label>' +
					messages.colon +
					' ' +
					'<input class="ed-word" name="word" size="20" title="' +
					messages.translationTooltip +
					'" placeholder="' +
					messages.translationPlaceholder +
					'"> ' +
					'<button type="submit">' +
					messages.addButton +
					'</button> ' +
					'<a href="#" class="ed-more">' +
					messages.moreButton +
					'</a></p>' +
					options +
					'<p><span class="ed-feedback">' +
					messages.feedbackButton +
					'</span></p>' +
					'<div class="ed-errors"></div>' +
					'<div class="ed-info-msg"></div>' +
					'</form>'
			)
		);

		// Make radio buttons deselectable
		form.find(':radio').click(function lastClick() {
			if (lastClick[this.name] === this) {
				lastClick[this.name] = null;
				this.checked = false;
			} else {
				lastClick[this.name] = this;
			}
		});

		var showAllOpts = false;
		form
			.find('.ed-lang-name')
			// If necessary, replace the language code with the language name.
			.blur(replaceLanguageCode)
			.blur(updateOptionsVisibility)

			// If the item exists, the value will be used as the value,
			// otherwise it's 'null', which empties (the already empty)
			// text field.
			.val(silentFailStorage.getItem('trans-lang'));
		form.find('.ed-more').click(function(e) {
			e.preventDefault();
			showAllOpts = !showAllOpts;
			$(this).text(showAllOpts ? messages.lessButton : messages.moreButton);
			updateOptionsVisibility();
		});
		form.find('.ed-feedback').click(function() {
			mw.loader.using('mediawiki.feedback', function() {
				var mwTitle = new mw.Title(parametrization.feedbackDestPage),
					feedback = new mw.Feedback({
						title: mwTitle
					});
				feedback.launch({
					subject: parametrization.feedbackFormDefaultHeader.replace(
						'<pagename>',
						mw.config.get('wgPageName').replace(/_/g, ' ')
					)
				});
				// Show a placeholder for the message.
				if ($('.mw-feedbackDialog-feedback-form textarea').length) {
					$('.mw-feedbackDialog-feedback-form textarea').get(0).placeholder =
						parametrization.feedbackFormMessagePlaceholder;
				}
				// Hide the button "Report a technical bug".
				$('.oo-ui-processDialog-actions-other').hide();
			});
		});
		updateOptionsVisibility();
		function updateOptionsVisibility() {
			var elems = form.find('.ed-options label');
			if (showAllOpts) {
				elems.show();
				form.find('.ed-feedback').show();

				// Only show the field for traditional script in Chinese and Korean.
				if (parametrization.conf.hasTraditionalCharactersField) {
					var langCode = form.attr('data-language-code');
					if (['zh', 'ko'].indexOf(langCode) === -1) {
						form.find('.ed-tradi').hide();
					}
				}
			} else {
				var opts = langMeta[form.attr('data-language-code')] || langMeta[''];
				elems
					.hide()
					.filter('.ed-' + opts.replace(/ /g, ', .ed-'))
					.show();
				form.find('.ed-feedback').hide();
			}
		}

		// Determine whether a language name is defined or not.
		// returns its index in langMap if the language exists, or false.
		function indexLangName(str) {
			if (langMap === undefined) {
				return undefined;
			}
			for (var i in langMap) {
				if (langMap[i].name === str) {
					return i;
				}
			}
			return false;
		}

		// Determine whether a language code is defined or not
		// returns its index in langMap if the code exists, or false.
		function indexLangCode(str) {
			if (langMap === undefined) {
				return undefined;
			}
			for (var i in langMap) {
				if (langMap[i].code === str) {
					return i;
				}
			}
			return false;
		}

		// Replace language code -> language name, run upon blurring the input field.
		function replaceLanguageCode() {
			// If the "preferUponConflict" behavior has been canceelled manually,
			// do not check right away the field for replacement
			if (form.attr('data-do-not-update-lang-on-blur')) {
				return;
			}
			var elem = form.find('.ed-lang-name');
			var val = elem.val();
			if (val === '') {
				elem.attr('title', messages.langNameTooltip);
				form.attr('data-language-code', '');
				form.attr('data-language-index', null);
				return;
			}

			// If the language name is capitalized, then uncapitalize
			if (val[0].toLowerCase() !== val[0]) {
				val = val[0].toLowerCase() + val.substr(1);
				elem.val(val);
			}

			// If a language name happens to be more often used as a language code,
			// then consider the language field content as a code to replace into a language name.
			// User should be warned of that replacement and its preference should be saved.
			var indexInputLangName = indexLangName(val);
			var indexInputLangCode = indexLangCode(val);
			if (indexInputLangName && indexInputLangCode) {
				var userPref = silentFailStorage.getItem('pref-lang-' + val);
				var wikiPref = langMap[indexInputLangCode]['preferUponConflict'];
				if (userPref || wikiPref) {
					form.attr({
						'data-language-code': userPref || val,
						'data-language-index':
							userPref === val ? indexInputLangName : indexInputLangCode
					});
					var preferredLangName =
						userPref || langMap[indexInputLangCode]['name'];
					var otherLangName = userPref
						? userPref === langMap[indexInputLangCode]['name']
							? val
							: langMap[indexInputLangCode]['name']
						: langMap[indexInputLangName]['name'];
					elem.val(preferredLangName);
					var msg = userPref
						? messages.savedPreferredLanguage
						: messages.langNameReplacedByACodeMessage;
					msg = msg
						.replace('<preferredLangName>', preferredLangName)
						.replace('<otherLangName>', otherLangName)
						.replace(
							'<click>',
							'<span class="ed-change-lang ed-click" data-new-lang="<otherLangName>">'.replace(
								'<otherLangName>',
								otherLangName
							)
						)
						.replace('</click>', '</span>');
					form.find('.ed-info-msg').html(getInfoHtml(msg));
					$('.ed-change-lang').click(function() {
						var lang = $(this).attr('data-new-lang');
						var index = indexLangName(lang);
						form.attr({
							'data-language-code': langMap[index].code,
							'data-language-index': index,
							'data-do-not-update-lang-on-blur': true
						});
						silentFailStorage.setItem('pref-lang-' + val, lang);
						elem.val(lang);
						elem.blur();
						form.attr('data-do-not-update-lang-on-blur', null);
						form.find('.ed-info-msg').empty();
					});
					silentFailStorage.setItem('pref-lang-' + val, preferredLangName);
					return;
				}
			}
			if (indexInputLangName) {
				form.attr({
					'data-language-code': langMap[indexInputLangName]['code'],
					'data-language-index': indexInputLangName
				});
				return;
			}
			if (indexInputLangCode) {
				form.attr({
					'data-language-code': val,
					'data-language-index': indexInputLangCode
				});
				elem.val(langMap[indexInputLangCode].name);
				return;
			}
			if (langCodeAliasMap[val]) {
				val = langCodeAliasMap[val];
				var indexLang = indexLangCode(val);
				form.attr({
					'data-language-code': val,
					'data-language-index': indexLang
				});
				elem.val(langMap[indexLang].name);
				return;
			}
			// If input doesn't correspond to a language code, a language name or the empty string
			form.attr({
				'data-language-code': null,
				'data-language-index': null
			});
			elem.attr('title', messages.langNameTooltip);
		}

		form.appendTo(navContent);

		navContent.find('input').focus(function() {
			editor.init();
			initLangs();
			enableAutocomplete(navContent);
		});

		var hasSeenCommaWarning = false;
		form.submit(function(e) {
			e.preventDefault();
			// Trim spaces in inputs
			var langName = $.trim(this.langName.value);
			var word = $.trim(this.word.value);
			var gender = form
				.find('.ed-gender input:checked')
				.prop('checked', false)
				.val();
			var trans =
				$.trim(this.trans.value) !==
				$.trim(this.trans.getAttribute('placeholder'))
					? $.trim(this.trans.value)
					: '';
			var tradi = '';
			if (parametrization.conf.hasTraditionalCharactersField) {
				tradi =
					$.trim(this.tradi.value) !==
					$.trim(this.tradi.getAttribute('placeholder'))
						? $.trim(this.tradi.value)
						: '';
			}
			var pagename =
				$.trim(this.pagename.value) !==
				$.trim(this.pagename.getAttribute('placeholder'))
					? $.trim(this.pagename.value)
					: '';

			silentFailStorage.setItem('trans-lang', langName);

			var title = mw.config.get('wgTitle');

			if (!langName) {
				showError(new NoInputError('lang-name'));
				return;
			} else if (!word) {
				showError(new NoInputError('word'));
				return;
			}
			// Comma or semicolon in the translation when the title doesn't contain one.
			else if (
				word.indexOf(',') !== -1 &&
				title.indexOf(',') === -1 &&
				!hasSeenCommaWarning
			) {
				showError(new CommaWordError(messages.errorMessageComma));
				hasSeenCommaWarning = true;
				return;
			} else if (
				word.indexOf('،') !== -1 &&
				title.indexOf('،') === -1 &&
				!hasSeenCommaWarning
			) {
				showError(new CommaWordError(messages.errorMessageComma));
				return;
			} else if (
				word.indexOf(';') !== -1 &&
				title.indexOf(';') === -1 &&
				!hasSeenCommaWarning
			) {
				showError(new CommaWordError(messages.errorMessageSemicolon));
				return;
			} else if (
				word.indexOf('/') !== -1 &&
				title.indexOf('/') === -1 &&
				!hasSeenCommaWarning
			) {
				showError(new CommaWordError(messages.errorMessageSlash));
				return;
			} else if (
				word[0] !== word[0].toLowerCase() &&
				title[0] === title[0].toLowerCase() &&
				langMap[this.getAttribute('data-language-index')].expectCapital &&
				silentFailStorage.getItem('case-knowledge') !== 'ok'
			) {
				showError(new CaseWordError());
				return;
			} else if (
				this.getAttribute('data-language-code') ===
				mw.config.get('wgContentLanguage')
			) {
				showError(new BadLangNameError());
				return;
			} else if (translation.reWikitext.test(word)) {
				showError(new BadFormatError());
				return;
			}

			var wordOptions = {
				langName: langName,
				langCode: null,
				word: word,
				exists: null,
				gender: gender,
				trans: trans,
				tradi: tradi,
				pagename: pagename
			};

			function showError(e) {
				form.find('.ed-error').removeClass('ed-error');
				if (!e) {
					form.find('.ed-errors').empty();
					return;
				}
				if (e instanceof NoLangTplError) {
					form
						.find('.ed-lang-name')
						.addClass('ed-error')
						.focus();
					e = messages.errorLangUndefined.replace('<lang>', e.langName);
				} else if (e instanceof NoInputError) {
					form
						.find('.ed-' + e.input)
						.addClass('ed-error')
						.focus();
					if (e.input === 'lang-name') {
						e = messages.errorLangMissing;
					} else if (e.input === 'word') {
						e = messages.errorTranslationMissing;
					}
				} else if (e instanceof BadLangNameError) {
					form
						.find('.ed-lang-name')
						.addClass('ed-error')
						.focus();
					e = messages.errorTranslationIntoWikiLanguageName;
				} else if (e instanceof CommaWordError) {
					form
						.find('.ed-word')
						.addClass('ed-error')
						.focus();
					e = messages.warningTranslationContainsUncommonCharacter
						.replace('<character>', e.message)
						.replace('<click>', '<span class="ed-error-comma ed-click">')
						.replace('</click>', '</span>');
				} else if (e instanceof CaseWordError) {
					form
						.find('.ed-word')
						.addClass('ed-error')
						.focus();
					e = messages.warningTranslationIsCapitalized
						.replace('<click>', '<span class="ed-error-case ed-click">')
						.replace('</click>', '</span>');
				} else if (e instanceof BadFormatError) {
					form
						.find('.ed-word')
						.addClass('ed-error')
						.focus();
					e = messages.errorTranslationContainsWikitext;
				} else if (e instanceof HttpError) {
					e = messages.errorHttpError;
				}
				form.find('.ed-errors').html(getErrorHtml(e));
				// In case of a detected comma.
				$('.ed-error-comma').click(function() {
					hasSeenCommaWarning = true;
					form.find(':submit').click();
				});
				// In case of a detected capital.
				$('.ed-error-case').click(function() {
					silentFailStorage.setItem('case-knowledge', 'ok');
					form.find(':submit').click();
				});
			}

			var langCode = form.attr('data-language-code');
			if (!langCode) {
				showError(new NoLangTplError(langName));
				return;
			}

			wordOptions.langCode = langCode;

			$.when(
				// wordHtml
				pageExists(langCode, pagename ? pagename : word).then(function(
					pageExists
				) {
					wordOptions.exists = pageExists;
					return parseWikitext(translation.getFormattedWord(wordOptions));
				}),
				// wikitext
				editor.wikitext()
			)
				.fail(function(error) {
					if (error === 'http') {
						// jQuery HTTP error
						showError(new HttpError());
					} else {
						showError(error);
					}
				})
				.then(function(wordHtml, wikitext) {
					showError(false);
					form.find('.ed-info-msg').empty();

					silentFailStorage.setItem('trans-lang', langName);

					form[0].word.value = '';
					form[0].trans.value = '';
					form[0].pagename.value = '';
					if (parametrization.conf.hasTraditionalCharactersField) {
						form[0].tradi.value = '';
					}

					var addedElem;
					var index = getTranslationTableIndex(table);

					wikitext = translation.add(wikitext, index, wordOptions);

					editor.edit({
						summary:
							'+' + langName + ' : [[' + (pagename ? pagename : word) + ']]',
						summaryType: 'transl',
						wikitext: wikitext,
						redo: function() {
							var translations = self.find('ul > li');
							translation.addToList({
								items: translations,
								addOnlyItem: function() {
									addedElem = $('<li>', {
										html:
											'<span class="trad-' +
											langCode +
											'">' +
											langName[0].toUpperCase() +
											langName.substr(1) +
											'</span> : ' +
											wordHtml
									});
									self.find('ul').prepend(addedElem);
								},
								equalOrBefore: function(item) {
									var match = /^\s*(.+)/.exec(
										$(item)
											.children()
											.first()
											.text()
									);
									if (match) {
										// International conventions first.
										if (
											match[1].toLowerCase() ===
												parametrization.translingualName &&
											langName !== parametrization.translingualName
										) {
											return 'before';
										}
										if (
											langName === parametrization.translingualName &&
											match[1].toLowerCase() !==
												parametrization.translingualName
										) {
											return false;
										}

										// Ignore unrelevant list item (eg. fr.wikt's {{ébauche-trad}})
										if (
											parametrization.itemsToIgnore &&
											parametrization.itemsToIgnore.length
										) {
											for (
												var i = 0;
												i < parametrization.itemsToIgnore.length;
												i++
											) {
												if (match[1] === parametrization.itemsToIgnore[i])
													return false;
											}
										}

										if (sortkey(match[1].toLowerCase()) === sortkey(langName)) {
											if ($(item).children('dl').length) {
												// If the language has dialects on different levels
												// (in wikicode, with a line starting with *:),
												// we cannot handle adding translations.
												mw.notify(messages.unmanagedTranslationsFormatting);
												throw new Error(
													'Error: format for translations in this language' +
														' is not handled by the gadget.'
												);
											}
											return 'equal';
										} else if (
											sortkey(match[1].toLowerCase()) < sortkey(langName)
										) {
											return 'before';
										}
									}
									return false;
								},
								addToItem: function(item) {
									addedElem = $('<span>', { html: ', ' + wordHtml }).appendTo(
										item
									);
								},
								addAfter: function(item) {
									addedElem = $('<li>', {
										html:
											'<span class="trad-' +
											langCode +
											'">' +
											langName[0].toUpperCase() +
											langName.substr(1) +
											'</span> : ' +
											wordHtml
									}).insertAfter(item);
								},
								addBefore: function(item) {
									addedElem = $('<li>', {
										html:
											'<span class="trad-' +
											langCode +
											'">' +
											langName[0].toUpperCase() +
											langName.substr(1) +
											'</span> : ' +
											wordHtml
									});
									addedElem.insertBefore(item);
								}
							});
							addedElem.addClass('ed-added');
							mw.hook('wikipage.content').fire(addedElem);
						},
						undo: function() {
							addedElem.remove();
						}
					});
				});
		});
	}

	// The map of languages <-> language codes is loaded, if it hasn't already been.
	function initLangs() {
		if (langMap) {
			return;
		}

		langMap = {};
		langNameArray = [];
		langCodeMap = {};
		// Map of lang code aliases, for a quick access when sorting translations
		langCodeAliasMap = {};

		// TODO: proxy the mw.api query to localhost to keep one version of the code
		var def = $.Deferred();
		$.getJSON('gadget/languages.json', function(data) {
			langMap = data;

			// Create array of language names for autocompletion.
			for (var i in langMap) {
				// we fill langNameArray
				langNameArray.push(langMap[i].name);
				if (langMap[i].nameAliases) {
					langNameArray.push(langMap[i].nameAliases);
				}
				// we fill langCodeMap
				langCodeMap[langMap[i].code] = langMap[i].name;
				// we fill langCodeAliasMap
				if (langMap[i].codeAliases) {
					langMap[i].codeAliases.forEach(function(codeAlias) {
						langCodeAliasMap[codeAlias] = langMap[i].code;
					});
				}
			}
			def.resolve();
		});
		return def.promise();

		/*var api = new mw.Api();
		return api
			.get({
				action: 'query',
				format: 'json',
				titles: 'MediaWiki:Gadget-translation editor.js/langues.json',
				prop: 'revisions',
				rvprop: 'content'
			})
			.then(function(data) {
				// Get the first (and only) page from data.query.pages
				for (var pageid in data.query.pages) break;
				langMap = JSON.parse(data.query.pages[pageid].revisions[0]['*']);
				langMap = JSON.parse(data);

				// Fill the array of language names for autocompletion.
				for (var langObj in langMap) {
					langNameArray.push(langObj.code);
					if (langObj.codeAliases) {
						langNameArray.push(langObj.codeAliases);
					}
				}
			});
		*/
	}

	// Autocompletion of language names.
	var autocompleteOptions = {
		source: function(request, response) {
			if (langNameArray) {
				var matcher = new RegExp(
					'^' + $.ui.autocomplete.escapeRegex(request.term),
					'i'
				);
				response(
					langNameArray.filter(function(item) {
						return matcher.test(item);
					})
				);
			}
		},
		minLength: 2
	};

	function enableAutocomplete(parent) {
		mw.loader.using('jquery.ui.autocomplete', function() {
			$(parent)
				.find('.ed-lang-name')
				.autocomplete(autocompleteOptions);
		});
	}

	function parseWikitext(wikitext) {
		return new mw.Api()
			.get({
				action: 'parse',
				text: '<div>' + wikitext + '</div>',
				title: wgPageName
			})
			.then(function(data) {
				var html = data.parse.text['*'];
				// Get only the parts between <div> and </div>
				html = html.substring(
					html.indexOf('<div>') + '<div>'.length,
					html.lastIndexOf('</div>')
				);
				return $.trim(html);
			});
	}

	function pageExists(langCode, page) {
		var domain,
			wmLinks = {
				cmn: 'zh',
				'fra-nor': 'nrm',
				gsw: 'als',
				'ko-Hani': 'ko',
				lzh: 'zh-classical',
				nan: 'zh-min-nan',
				nb: 'no',
				nn: 'no',
				rup: 'roa-rup',
				yue: 'zh-yue'
			},
			// prettier-ignore
			wiktionaries = [
				'en', 'mg', 'fr', 'zh', 'lt', 'ru', 'es', 'el', 'pl', 'sv', 'ko',
				'nl', 'de', 'tr', 'ku', 'ta', 'io', 'kn', 'fi', 'vi', 'hu', 'pt',
				'chr', 'no', 'ml', 'my', 'id', 'it', 'li', 'ro', 'et', 'ja', 'te',
				'jv', 'fa', 'cs', 'ca', 'ar', 'eu', 'gl', 'lo', 'uk', 'br', 'fj',
				'eo', 'bg', 'hr', 'th', 'oc', 'is', 'vo', 'ps', 'zh-min-nan',
				'simple', 'cy', 'uz', 'scn', 'sr', 'af', 'ast', 'az', 'da', 'sw',
				'fy', 'tl', 'he', 'nn', 'wa', 'ur', 'la', 'sq', 'hy', 'sm', 'sl',
				'ka', 'pnb', 'nah', 'hi', 'tt', 'bs', 'lb', 'lv', 'tk', 'sk', 'hsb',
				'nds', 'kk', 'ky', 'be', 'km', 'mk', 'ga', 'wo', 'ms', 'ang', 'co',
				'sa', 'gn', 'mr', 'csb', 'ug', 'st', 'ia', 'sd', 'sh', 'si', 'mn',
				'tg', 'or', 'kl', 'vec', 'jbo', 'an', 'ln', 'fo', 'zu', 'gu', 'kw',
				'gv', 'rw', 'qu', 'ss', 'ie', 'mt', 'om', 'bn', 'pa', 'roa-rup',
				'iu', 'so', 'am', 'su', 'za', 'gd', 'mi', 'tpi', 'ne', 'yi', 'ti',
				'sg', 'na', 'dv', 'tn', 'ts', 'ha', 'ks', 'ay'
			];

		if (wmLinks[langCode]) {
			domain = wmLinks[langCode] + '.wiktionary';
		} else if (langCode === 'conv') {
			domain = 'species.wikimedia';
		} else if (wiktionaries.indexOf(langCode) !== -1) {
			domain = langCode + '.wiktionary';
		}

		// If the Wiktionary doesn't exist, it's useless to make an HTTP request.
		if (!domain) {
			return $.Deferred()
				.resolve('trad--')
				.promise();
		}

		// Convert typographical apostrophes into ASCII ones,
		// since many wikis prefer the latter.
		page = page.replace(/’/g, "'");

		var def = $.Deferred();
		$.ajax({
			// prettier-ignore
			url: '//' + domain + '.org/w/api.php?origin=' + location.protocol + '//' + location.host,
			data: {
				action: 'query',
				titles: page,
				format: 'json'
			},
			dataType: 'json'
		})
			.fail(function() {
				def.resolve('trad');
			})
			.then(function(data) {
				def.resolve(data.query.pages[-1] ? 'trad-' : 'trad+');
			});
		return def.promise();
	}

	function sortkey(word) {
		var key = word.toLowerCase();
		key = key.replace(/[àáâãäå]/g, 'a');
		key = key.replace(/[æ]/g, 'ae');
		key = key.replace(/[çćċč]/g, 'c');
		key = key.replace(/[ĉ]/g, 'cx');
		key = key.replace(/[èéêë]/g, 'e');
		key = key.replace(/[ĝ]/g, 'gx');
		key = key.replace(/[ĥ]/g, 'hx');
		key = key.replace(/[ìíîï]/g, 'i');
		key = key.replace(/[ĵ]/g, 'jx');
		key = key.replace(/[ñ]/g, 'n');
		key = key.replace(/[òóôõö]/g, 'o');
		key = key.replace(/[œ]/g, 'oe');
		key = key.replace(/[òóôõö]/g, 'o');
		key = key.replace(/[ŝ]/g, 'sx');
		key = key.replace(/[ùúûü]/g, 'u');
		key = key.replace(/[ŭ]/g, 'ux');
		key = key.replace(/[ýÿ]/g, 'y');
		key = key.replace(/['’)(]/g, '');
		key = key.replace(/[-/]/g, ' ');
		return key;
	}

	function getTranslationObject() {
		return {
			reWikitext: /[[\]{}#|=]/,

			containsWikitext: function(str) {
				return translation.reWikitext.test(str);
			},

			reGloss: new RegExp(
				'{{' + parametrization.translation.transTopTemplate + '(.*)}}',
				'g'
			),

			reSection: new RegExp(
				'({{' +
					parametrization.translation.transTopTemplate +
					'.*}})([\\s\\S]*?)({{' +
					parametrization.translation.transBottomTemplate +
					'}})',
				'g'
			),

			isTransTop: function(gloss) {
				return gloss.replace(translation.reGloss, '-') === '-';
			},

			makeTransTop: function(gloss) {
				if (translation.isTransTop(gloss)) {
					return gloss;
				} else {
					return parametrization.translation.topTransTemplateWithGloss.replace(
						'<gloss>',
						gloss
					);
				}
			},

			getGloss: function(wikitext, index) {
				// If there's a comment, the analysis isn't possible.
				if (
					new RegExp(
						'{{' +
							parametrization.translation.transTopTemplate +
							'\\|.*?(?:<!--|-->)'
					).test(wikitext)
				) {
					mw.notify(messages.translation.errorNotificationHTMLCommentInHeader);
					throw new Error(
						messages.translation.errorNotificationHTMLCommentInHeader
					);
				}
				translation.reGloss.lastIndex = 0;

				for (var i = 0; i <= index; i++) {
					var match = translation.reGloss.exec(wikitext);
					if (i === index && match) {
						var standard = /^(|\|[^|=]*)$/.test(match[1]);
						return {
							transTop: match[0],
							text: standard ? match[1].substr(1) : void 0,
							standard: standard
						};
					}
				}
				throw new Error(
					messages.translation.errorNthTransTopTemplateNotFound
						.replace('<index>', index + 1)
						.replace(
							'<transTopTemplate>',
							parametrization.translation.transTopTemplate
						)
				);
			},
			setGloss: function(wikitext, index, gloss) {
				index++;
				var count = 0;
				return wikitext.replace(translation.reGloss, function(match) {
					count++;
					if (count !== index) {
						return match;
					}
					return translation.makeTransTop(gloss);
				});
			},
			getFormattedWord: function(opts) {
				var tpl = [
					opts.exists,
					opts.langCode,
					opts.pagename ? opts.pagename : opts.word
				];
				opts.gender && tpl.push(opts.gender);
				opts.trans &&
					tpl.push(
						parametrization.translation.transTemplateParameters.trans +
							opts.trans
					);
				opts.tradi &&
					tpl.push(
						parametrization.translation.transTemplateParameters.tradi +
							opts.tradi
					);
				opts.pagename &&
					tpl.push(
						parametrization.translation.transTemplateParameters.displayed +
							opts.word
					);
				var res = '{{' + tpl.join('|') + '}}';
				opts.langCode === 'conv' && (res = "''" + res + "''"); // International conventions in italic.
				return res;
			},
			// Options:
			// - items: Array of items
			// - equalOrBefore: Function that returns either 'equal', 'before' or false
			// - addToItem: Adds a word to an item
			// - addAfter: Adds the item after an item
			// - addBefore: Adds the item before an item
			addToList: function(opts) {
				var items = opts.items;
				if (!items.length) {
					items[0] = opts.addOnlyItem();
					return items;
				}
				for (var i = items.length - 1; i >= 0; i--) {
					var eqOrBef = opts.equalOrBefore(items[i]);
					if (eqOrBef === 'equal') {
						items[i] = opts.addToItem(items[i]);
						return items;
					} else if (eqOrBef === 'before') {
						items[i] = opts.addAfter(items[i]);
						return items;
					}
				}
				items[0] = opts.addBefore(items[0]);
				return items;
			},
			add: function(wikitext, index, opts) {
				var match = wikitext.match(translation.reSection);
				if (match === null) {
					mw.notify(
						messages.translation.errorNotificationtransBottomMissing.replace(
							'<transBottomTemplate>',
							parametrization.translation.transBottomTemplate
						)
					);
					throw new Error(
						"Page can't be edited manually, a bottom translation template seems missing"
					);
				}
				if (match[index].indexOf('<!--') !== -1) {
					mw.notify(
						messages.translation.errorNotificationHTMLCommentInTranslations
					);
					throw new Error(
						messages.translation.errorNotificationHTMLCommentInTranslations
					);
				}
				index++;
				var count = 0;
				return wikitext.replace(translation.reSection, function(
					match,
					p1,
					p2,
					p3
				) {
					count++;
					if (count !== index) {
						return match;
					}

					p2 = $.trim(p2);

					var formattedWord = translation.getFormattedWord(opts);

					var lines = translation.addToList({
						// split into lines
						items: p2 ? p2.split('\n') : [],
						addOnlyItem: function() {
							return parametrization.translation.newTranslationFormat
								.replace('<lang code>', opts.langCode)
								.replace('<lang name>', opts.langName)
								.replace('<word>', formattedWord);
						},
						equalOrBefore: function(line) {
							var matchTransl = new RegExp(
								parametrization.translation.regexMatchTranslation
							).exec(line);
							// TODO: special case; review similar cases in other wikis
							// Also take into account rare cases which are indented on a line using *: {{trad|...}}
							// Example: Chinese on [[arrière-grand-mère]]
							var matchTranslIndented = new RegExp(
								'^\\*:\\s*{{trad[+-]{0,2}\\|([^}|]+?)\\|'
							).exec(line);
							if (matchTransl || matchTranslIndented) {
								var match = matchTransl || matchTranslIndented;
								var langCode = match[1];
								// International conventions first.
								if (langCode === 'conv' && opts.langCode !== 'conv') {
									return 'before';
								}
								if (opts.langCode === 'conv' && langCode !== 'conv') {
									return false;
								}

								// The language code in the template T can be a redirect to another code,
								// in which case we'll use the target code.
								// Redirects are found as a property "redirects" on the list of languages
								// which contains all language code redirects.
								if (!langCodeMap[langCode]) {
									if (langCodeAliasMap[langCode]) {
										langCode = langCodeAliasMap[langCode];
									} else {
										// The code is not defined.
										return false;
									}
								}
								var langName = langCodeMap[langCode];
								if (sortkey(langName) === sortkey(opts.langName)) {
									// TODO: special case; review similar cases in other wikis
									// If a translation line ends with ":"
									// (and thus only includes the language name without translation afterwards),
									// it is probably that we are dealing with a language with dialects
									// that will be introduced on a new line by "*:" → case to be managed manually.
									if (
										/^\*\s*\{\{T\|[^}|]+?(?:\|trier)?\}\}\s*:\s*$/.exec(line)
									) {
										mw.notify(messages.translation.errorWeirdFormat);
										throw new Error(
											"Weird format: wikicode for this translation's target language " +
												' is not managed.'
										);
									}
									return 'equal';
								} else if (sortkey(langName) < sortkey(opts.langName)) {
									return 'before';
								}
							}
							return false;
						},
						addToItem: function(line) {
							return line + ', ' + formattedWord;
						},
						addBefore: function(line) {
							return this.addOnlyItem() + '\n' + line;
						},
						addAfter: function(line) {
							return line + '\n' + this.addOnlyItem();
						}
					});

					return p1 + '\n' + lines.join('\n') + '\n' + p3;
				});
			}
		};
	}

	function extendError(name, p1Name) {
		function E(p1) {
			this.message = p1;
			if (p1Name) this[p1Name] = p1;
		}
		E.prototype = new Error();
		E.prototype.constructor = E;
		E.prototype.name = name;
		return E;
	}

	var NoLangTplError = extendError('NoLangTplError', 'langName');
	var NoInputError = extendError('NoInputError', 'input');
	var BadLangNameError = extendError('BadLangNameError');
	var CommaWordError = extendError('CommaWordError');
	var CaseWordError = extendError('CaseWordError');
	var BadFormatError = extendError('BadFormatError');
	var HttpError = extendError('HttpError');

	// Export some useful components
	window.translation = translation;
	window.parseWikitext = parseWikitext;
	window.addHeadingUpdater = addHeadingUpdater;
	window.addTranslationForm = addTranslationForm;
})(mediaWiki, jQuery);
