/**
 * Local storage wrapper
 *
 * If localStorage is supported, that is what's used, otherwise a no-op implementation is provided.
 */
window.silentFailStorage = (function() {
	try {
		// Will throw if localStorage isn't supported or if it's disabled
		var l = window.localStorage,
			val = l.getItem('a');
		l.setItem('a', 'b');
		if (l.getItem('a') === 'b') {
			// it works
			if (val !== null) {
				l.setItem('a', val);
			} else {
				l.removeItem('a');
			}
			return l;
		}
	} catch (e) {
		// Fail silently
	}

	function noop() {}
	function rnull() {
		return null;
	}

	return {
		getItem: rnull,
		setItem: noop,
		removeItem: noop,
		clear: noop,
		key: rnull,
		length: 0
	};
})();
