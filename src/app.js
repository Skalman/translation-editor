const path = require('path');
const url = require('url');
const https = require('https');
const express = require('express');

// Settings
const PORT = +process.env.EDITOR_PORT || 3000;
const USER_AGENT =
	process.env.EDITOR_USER_AGENT ||
	'Translation editor (authors: Skalman, Automatik)';

const app = express();
app.get('/', (_req, res) => res.redirect('./index.html'));
app.get('/favicon.ico', (_req, res) => res.redirect('./images/favicon.png'));

// Proxy API calls and other wiki requests. Only allow GET for security.
app.get(/^\/(w|api)\//, (req, res) => {
	// Load the languages list directly from the staging environment: ./gadget/languages.json
	const jsonLangsFileName =
		'MediaWiki:Gadget-translation editor.js/langues.json';
	if (req.query.titles === jsonLangsFileName) {
		res.type('json');
		var options = {
			root: __dirname + '/public/',
			dotfiles: 'deny',
			headers: {
				'x-timestamp': Date.now(),
				'x-sent': true
			}
		};
		res.sendFile('gadget/languages.json', options, function(err) {
			if (err) {
				new Error('Error while sending JSON languages list!');
			} else {
				console.log('Sent:', 'languages.json');
			}
		});
	} else {
		proxy('fr.wiktionary.org', req.url, req, res);
	}
});

app.get('/proxy', (req, res) => {
	const parsedUrl = url.parse(req.query.url);

	if (parsedUrl.protocol !== 'https:') {
		throw new Error('Protocol must be HTTPS');
	}

	proxy(parsedUrl.host, parsedUrl.path, req, res);
});

/**
 *
 * @param {string} targetHost
 * @param {string} targetPath
 * @param {Request} req
 * @param {Response} res
 */
function proxy(targetHost, targetPath, req, res) {
	const isAllowedHost =
		targetHost.endsWith('.wiktionary.org') ||
		targetHost === 'species.wikimedia.org';

	if (!isAllowedHost) {
		throw new Error(`Proxying to host '${targetHost}' is not allowed`);
	}

	https
		.request(
			{
				method: req.method,
				host: targetHost,
				path: targetPath,
				headers: {
					Accept: req.header('Accept'),
					'User-Agent': USER_AGENT
				}
			},
			cres => {
				res.writeHead(cres.statusCode, cres.headers);
				cres
					.setEncoding('utf8')
					.on('data', chunk => res.write(chunk))
					.on('close', () => res.end())
					.on('end', () => res.end());
			}
		)
		.on('error', e => {
			console.log(e.message);
			res.writeHead(500);
			res.end();
		})
		.end();
}

app.use(express.static(path.join(__dirname, 'public')));

app.listen(PORT, () =>
	console.log(`Server started on http://localhost:${PORT}`)
);
